declare namespace reign {
  interface Response {
    status: number
    message: string
  }
}

declare module 'reign' {
  export = reign;
}