import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';

describe('ArticlesController (e2e)', () => {
  let app: INestApplication;
  let objectId: number;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule]
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/articles (GET)', async (done) => {
    const response = await request(app.getHttpServer())
      .get('/articles')
      .expect(200);
    const { articles } = response.body;
    expect(articles).toBeInstanceOf(Array);
    objectId = +articles[0].objectId;
    expect(objectId).toEqual(expect.any(Number));
    return done();
  });

  it('/articles (DELETE', async (done) => {
    const response = await request(app.getHttpServer())
      .delete(`/articles/${objectId}`)
      .expect(200);
    const { deleted } = response.body;
    expect(deleted).toBeTruthy();
    return done();
  });

  afterAll(async () => {
    await Promise.all([app.close()]);
  });
});
