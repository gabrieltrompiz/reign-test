import { Injectable, OnApplicationBootstrap } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { Article, ArticleDocument } from "./article.schema";
import { Cron, CronExpression } from "@nestjs/schedule";
import axios from 'axios';
import * as camelcaseKeys from 'camelcase-keys';

@Injectable()
export class ArticlesService implements OnApplicationBootstrap {
  constructor(@InjectModel(Article.name) private articleModel: Model<ArticleDocument>) {}

  async onApplicationBootstrap(): Promise<void> {
    await this.fetchFromAPI();
  };

  async findAll(): Promise<Article[]> {
    return await this.articleModel.find({ deleted: false }).sort({ createdAt: -1 }).exec();
  }

  async fetchFromAPI(): Promise<void> {
    const response = await axios.get<{ hits: Article[] }>('http://hn.algolia.com/api/v1/search_by_date?query=nodejs');
    const articles = response.data.hits.map(article => {
      const formattedArticle = camelcaseKeys(article);
      formattedArticle._id = formattedArticle.objectId;
      return this.articleModel.findOneAndUpdate({ _id: formattedArticle._id }, formattedArticle, { upsert: true, setDefaultsOnInsert: true });
    });
    await Promise.all(articles);
  }

  async delete(id: number): Promise<boolean> {
    const deleted = await this.articleModel.updateOne({ objectId: id }, { deleted: true });
    return !!deleted.nModified;
  }

  @Cron(CronExpression.EVERY_HOUR)
  async refresh(): Promise<void> {
    await this.fetchFromAPI();
  }  
}