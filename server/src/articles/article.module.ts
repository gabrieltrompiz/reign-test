import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { ArticlesController } from "./article.controller";
import { Article, ArticleSchema } from "./article.schema";
import { ArticlesService } from "./article.service";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Article.name, schema: ArticleSchema }])
  ],
  controllers: [ArticlesController],
  providers: [ArticlesService]
})
export class ArticlesModule { }