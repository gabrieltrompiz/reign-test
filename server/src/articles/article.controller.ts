import { Controller, Delete, Get, Param } from "@nestjs/common";
import { Response } from "reign";
import { Article } from "./article.schema";
import { ArticlesService } from "./article.service";

@Controller('articles')
export class ArticlesController {
  constructor(private articlesService: ArticlesService) {}

  @Get()
  async findAll(): Promise<{ articles: Article[] } & Response> {
    const articles = await this.articlesService.findAll();
    return { 
      status: 200,
      message: 'Articles returned successfully.',
      articles
    };
  }

  @Delete(':id')
  async delete(@Param('id') id: number): Promise<{ deleted: boolean } & Response> {
    const deleted = await this.articlesService.delete(id);
    return {
      status: 200,
      message: 'Article deleted successfully',
      deleted
    }
  }
}