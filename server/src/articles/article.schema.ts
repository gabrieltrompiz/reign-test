import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from 'mongoose';

export type ArticleDocument = Article & Document;

@Schema()
export class Article {
  @Prop()
  _id: Number

  @Prop()
  author: string

  @Prop()
  commentText: string

  @Prop()
  createdAt: string

  @Prop()
  objectId: number

  @Prop()
  storyId: number

  @Prop()
  storyText: string

  @Prop()
  title: string

  @Prop()
  url: string

  @Prop()
  storyTitle: string

  @Prop()
  storyUrl: string

  @Prop({ default: false })
  deleted: boolean
}

export const ArticleSchema = SchemaFactory.createForClass(Article);