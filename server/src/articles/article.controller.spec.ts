import { ScheduleModule } from '@nestjs/schedule';
import { Test, TestingModule } from '@nestjs/testing';
import { ArticlesController } from './article.controller';
import { ArticlesService } from './article.service';

describe('ArticlesController', () => {
  let articlesController: ArticlesController;
  let articlesService = { 
    findAll: () => ['test'],
    delete: (id: number) => !!id
  };

  beforeAll(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [ArticlesController],
      providers: [ArticlesService],
    })
      .overrideProvider(ArticlesService)
      .useValue(articlesService)
      .compile();

    articlesController = app.get<ArticlesController>(ArticlesController);
  });

  describe('root', () => {
    it('should return array of articles', async (done) => {
      const { articles } = await articlesController.findAll();
      expect(Array.isArray(articles)).toBeTruthy();
      done();
    });

    it('should delete existent article', async (done) => {
      const { deleted } = await articlesController.delete(1);
      expect(deleted).toBeTruthy();
      done();
    })

    it('should return false when deleted article does not exists', async (done) => {
      const { deleted } = await articlesController.delete(0);
      expect(deleted).toBeFalsy();
      done();
    });
  });
});
