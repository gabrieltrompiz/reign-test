import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ArticlesModule } from './articles/article.module';
import { ScheduleModule } from "@nestjs/schedule";

require('dotenv').config();

@Module({
  imports: [
    MongooseModule.forRoot(process.env.DB_CONNECTION_STRING, { authSource: 'admin', useFindAndModify: false }), 
    ArticlesModule,
    ScheduleModule.forRoot()
  ]
})
export class AppModule {}
