declare namespace reign {
  interface Article {
    author: string
    commentText: string
    createdAt: string
    objectId: number
    storyId: number
    storyText: string
    title: string
    url: string
    storyTitle: string
    storyUrl: string
    deleted: boolean
  }
}

declare module 'reign' {
  export = reign;
}