import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import reportWebVitals from './reportWebVitals';
import axios from 'axios';
import moment from 'moment';

axios.defaults.baseURL = process.env.REACT_APP_SERVER_URL;
moment.updateLocale('en', {
  calendar: {
    lastDay: '[Yesterday]',
    sameDay: 'HH:MM a',
    lastWeek: 'MMM D'
  }
})

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
