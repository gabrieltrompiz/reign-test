import axios from "axios";
import { Article } from "reign";

export const getArticles = async (): Promise<Article[]> => {
  const response = await axios.get<{ articles: Article[] }>('/articles');
  return response.data.articles;
};

export const deleteArticle = async(id: number): Promise<boolean> => {
  const response = await axios.delete<{ deleted: boolean }>(`/articles/${id}`);
  return response.data.deleted;
};