import React from 'react';
import { Article as ArticleI } from 'reign';
import styled from 'styled-components';
import moment from 'moment';
import TrashIcon from './TrashIcon';

const Wrapper = styled.div`
  background-color: #FFF;
  font-size: 13pt;
  display: flex;
  justify-content: space-between;
  border: 1px #CCC solid;
  border-right: none;
  border-left: none;
  margin: 0px 50px;
  padding: 20px 0px;
  font-family: Arial, Helvetica, sans-serif;

  &:hover {
    background-color: #FAFAFA;
    cursor: pointer;
  }
`;

const Title = styled.span`
  color: #333;
`;

const Author = styled.span`
  color: #999;
`;

const Time = styled(Title)``;

const TimeWrapper = styled.div`
  display: flex;
`;

const Article: React.FC<ArticleProps> = ({ storyTitle, title, author, createdAt, url, storyUrl, deleteArticle, objectId }) => {
  const handleClick = (e: React.MouseEvent<SVGSVGElement, MouseEvent>, id: number) => {
    e.stopPropagation();
    deleteArticle(id);
  }

  return (
    <Wrapper onClick={() => window.open(storyUrl || url, '_blank')}>
      <div>
        <Title>{storyTitle || title}</Title>
        <Author> - {author} - </Author>
      </div>
      <TimeWrapper>
        <Time>{moment(createdAt).calendar()}</Time>
        <TrashIcon onClick={(e) => handleClick(e, objectId)}/>
      </TimeWrapper>
    </Wrapper>
  );
};

export default Article;

interface ArticleProps extends ArticleI {
  deleteArticle: (id: number) => Promise<void>
}