import React, { useEffect, useState } from 'react';
import { Article as ArticleI } from 'reign';
import styled from 'styled-components';
import { deleteArticle, getArticles } from '../services/articles';
import Article from './Article';

const Wrapper = styled.div`
  flex: 1;
  overflow-y: auto;
`;

const News: React.FC = () => {
  const [articles, setArticles] = useState<ArticleI[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    fetchArticles();
  }, []);

  const fetchArticles = async () => {
    setLoading(true)
    try {
      const articles = await getArticles();
      setArticles(articles);
    } catch(e) {
      // Handle and display error message (toast)
    } finally {
      setLoading(false)
    }
  };

  const removeArticle = async (id: number) => {
    try {
      const deleted = await deleteArticle(id);
      if(deleted) {
        setArticles(articles => articles.filter(a => a.objectId !== id));
      } else {
        // Handle article not being found
      }
    } catch(e) {
      // Handle and display error message (toast)
    }
  };

  return (
    <Wrapper>
      {/* Show loading overlay when request is being made */}
      {loading && <></>}
      {articles.map(article => <Article {...article} key={article.objectId} deleteArticle={removeArticle} />)}
    </Wrapper>
  );
};

export default News;