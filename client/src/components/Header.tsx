import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  background-color: #404040;
  width: 100%;
  height: 250px;
  color: white;
  display: flex;
  flex-direction: column;
  justify-content: center;
  font-family: 'Helvetica Neue', Arial, Helvetica, sans-serif;
`;

const Title = styled.p`
  font-size: 50px;
  font-weight: bold;
  margin: 0;
  margin-left: 50px;
`;

const SubTitle = styled.p`
  font-size: 30px;
  margin: 0;
  margin-left: 50px;
`;

const Header: React.FC = () => {
  return (
    <Wrapper>
      <Title>HN Feed</Title>
      <SubTitle>{'We <3 hacker news!'}</SubTitle>
    </Wrapper>
  );
};

export default Header;