To run the project simply run the followind command while being on the root folder of the project:

```docker-compose up -d```

And open http://localhost in your preferred browser when the docker containers are all created and running.

By default client is routed to port 80, server to port 8080 and mongodb to port 27017.

Thanks for the opportunity! Looking forward for your response.